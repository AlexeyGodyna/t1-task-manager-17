package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.ITaskRepository;
import ru.t1.godyna.tm.api.service.ITaskService;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.TaskNotFoundException;
import ru.t1.godyna.tm.exception.field.DescriptionEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.exception.field.NameEmptyException;
import ru.t1.godyna.tm.exception.field.TaskIdEmptyException;
import ru.t1.godyna.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        add(task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        final Task task = taskRepository.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        final Task task = findOneByIndex(index);
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        final Task task = findOneById(id);
        task.setStatus(status);
        return task;
    }

    @Override
    public Integer getSize() {
        return taskRepository.getSize();
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
