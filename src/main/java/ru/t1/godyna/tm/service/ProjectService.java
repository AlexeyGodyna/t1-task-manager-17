package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.IProjectRepository;
import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.field.DescriptionEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.exception.field.NameEmptyException;
import ru.t1.godyna.tm.exception.field.ProjectIdEmptyException;
import ru.t1.godyna.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        add(project);
        return project;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        final Project project = projectRepository.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final Project project = projectRepository.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        final Project project = findOneByIndex(index);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        final Project project = findOneById(id);
        project.setStatus(status);
        return project;
    }

    @Override
    public Integer getSize() {
        return projectRepository.getSize();
    }

    @Override
    public boolean existsById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return projectRepository.existsById(projectId);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
