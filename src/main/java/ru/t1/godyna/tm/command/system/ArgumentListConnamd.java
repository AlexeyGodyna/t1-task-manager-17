package ru.t1.godyna.tm.command.system;

import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListConnamd extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (ICommand command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument.toString());
        }
    }

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Show list arguments";
    }

}
