package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        final StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (final Task task: tasks) {
            if (task == null) continue;
            stringBuilder.append(index + ". ");
            stringBuilder.append(task.getName() + " : ");
            stringBuilder.append(task.getDescription() + " : ");
            stringBuilder.append(task.getId() + " : ");
            stringBuilder.append(task.getProjectId());
            System.out.println(stringBuilder);
            index++;
            stringBuilder.setLength(0);
        }
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

}
