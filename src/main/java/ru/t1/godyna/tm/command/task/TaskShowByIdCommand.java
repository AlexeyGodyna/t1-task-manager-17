package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskShowCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Display task by id.";
    }

}
