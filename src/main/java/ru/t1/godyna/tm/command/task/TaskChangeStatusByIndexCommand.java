package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(index, status);
    }

    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

}
