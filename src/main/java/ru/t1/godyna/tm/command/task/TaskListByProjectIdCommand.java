package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskListCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public String getDescription() {
        return "Show task list by project id.";
    }

}
