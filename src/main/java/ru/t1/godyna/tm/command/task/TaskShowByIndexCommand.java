package ru.t1.godyna.tm.command.task;

import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskShowCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Display task by index.";
    }

}
