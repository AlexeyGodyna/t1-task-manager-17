package ru.t1.godyna.tm.command.project;

import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

}
