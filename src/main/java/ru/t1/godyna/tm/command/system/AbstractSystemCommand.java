package ru.t1.godyna.tm.command.system;

import ru.t1.godyna.tm.api.service.ICommandService;
import ru.t1.godyna.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
