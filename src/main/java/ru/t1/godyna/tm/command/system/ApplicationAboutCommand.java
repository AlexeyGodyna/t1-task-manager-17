package ru.t1.godyna.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Alexey Godyna");
        System.out.println("E-mail: agodyna@t1-consulting.ru");
        System.out.println("E-mail: algo2791@yandex.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Shows developer info";
    }

}
