package ru.t1.godyna.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand{

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

}
